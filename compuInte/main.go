package main

import (
	"fmt"
	"sync"
	"time"
)

type Service struct {
	Inprogress map[int]bool
	isPending map[int][]chan int
	lock sync.RWMutex
}

func ExpensiveFibonacci (n int) int {
	fmt.Printf("Calculate expensive fibonacci for %d\n", n)
	time.Sleep(5 * time.Second)
	return n
}

func (s *Service) Work(job int) {
	s.lock.RLock()
	exists := s.Inprogress[job]
	if exists {
		s.lock.RUnlock()
		response := make(chan int)
		defer close(response)

		s.lock.Lock()
		s.isPending[job] = append(s.isPending[job], response)
		s.lock.Unlock()
		fmt.Printf("Waiting for response job %d\n", job)
		resp := <-response
		fmt.Printf("Response Done, received %d\n ", resp)

		return 
	}
	s.lock.RUnlock()

	s.lock.Lock()
	s.Inprogress[job] = true
	s.lock.Unlock()
	
	fmt.Printf("Calculate Fibonacci for %d\n", job)
	result  := ExpensiveFibonacci(job)

	s.lock.RLock()
	pendingWorkers, exists := s.isPending[job]
	s.lock.RUnlock()

	if exists {
		for _, pending := range pendingWorkers {
			pending <- result
		}
		fmt.Printf("Result sent - all pending workers ready job %d\n", job)
	}

	s.lock.Lock()
	s.Inprogress[job] = false
	s.isPending[job] = make([]chan int, 0)
	s.lock.Unlock()
}

func NewService() *Service {
	return &Service{
		Inprogress: make(map[int]bool),
		isPending: make(map[int][]chan int),
	}
}

func main()  {
	service := NewService()
	jobs := []int{3,4,5,5,4,8,8,8}
	var wg sync.WaitGroup
	wg.Add(len(jobs))

	for _,  n := range jobs {
		go func(job int)  {
			defer wg.Done()
			service.Work(job)
		}(n)
	}

	wg.Wait()
}