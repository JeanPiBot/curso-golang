package main

import (
	"fmt"
	"sync"
	"time"
)

type DataBase struct {}

func (DataBase) createSingleConnection() {
	fmt.Println("Creating singleton for database")
	time.Sleep(2 * time.Second)
	fmt.Println("Creation Done")
}

var db *DataBase
var lock sync.Mutex

func getDataBaseInstance() *DataBase {
	lock.Lock()
	defer lock.Unlock()
	if db == nil {
		fmt.Println("Creating db connection")
		db = &DataBase{}
		db.createSingleConnection()
	} else {
		fmt.Println("Db already created")
	}
	return db
}

func main() {
	var wg sync.WaitGroup
	for i := 0; i < 10; i++ {
		wg.Add(1)
		go func()  {
			defer wg.Done()
			getDataBaseInstance()	
		}()
	}
	wg.Wait()
}