package main

import (
	"fmt"
	"net"
)


func main() {
	for i := 0; i < 100; i++ {
		//1, 2, ..4, 99
		conne, err := net.Dial("tcp", fmt.Sprintf("%s:%d", "scanema.name.org", i))

		if err != nil {
			continue
		}
		conne.Close()
		fmt.Printf("port %d is open\n", i)
	}
}