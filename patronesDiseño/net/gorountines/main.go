package main

import (
	"flag"
	"fmt"
	"net"
	"sync"
)

var site =  flag.String("site", "scanema.name.org", "url to scan")

func main() {
	flag.Parse()
	var wg sync.WaitGroup
	for i := 0; i < 65535; i++ {
		wg.Add(1)
		go func (port int)  {
			defer wg.Done()
			conne, err := net.Dial("tcp", fmt.Sprintf("%s:%d", *site, port))

			if err != nil {
				return
			}
			conne.Close()
			fmt.Printf("port %d is open\n", port)
		}(i)
	}
	wg.Wait()
}